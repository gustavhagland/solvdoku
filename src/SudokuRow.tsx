import { Grid } from '@mui/material'
import { ReactElement } from 'react'
import SudokuNumber from './SudokuNumber'

type Props = {
    /**
     * xijk at row i
     */
    numberPlaced: number[][]
    onChange: (column: number, number: number | undefined) => void
}
const SudokuRow = (props: Props): ReactElement => {
    const getNumber = (binary: number[]): number | undefined => {
        const index = binary.findIndex((val) => val === 1)
        const number = index === -1 ? undefined : index + 1
        console.log('got number: ' + number)
        return number
    }
    return (
        <Grid container columnSpacing={2}>
            <Grid container item xs={4}>
                <Grid item xs={4}>
                    <SudokuNumber
                        number={getNumber(props.numberPlaced[0])}
                        onChange={(number) => props.onChange(0, number)}
                    />
                </Grid>
                <Grid item xs={4}>
                    <SudokuNumber
                        number={getNumber(props.numberPlaced[1])}
                        onChange={(number) => props.onChange(1, number)}
                    />
                </Grid>
                <Grid item xs={4}>
                    <SudokuNumber
                        number={getNumber(props.numberPlaced[2])}
                        onChange={(number) => props.onChange(2, number)}
                    />
                </Grid>
            </Grid>
            <Grid container item xs={4}>
                <Grid item xs={4}>
                    <SudokuNumber
                        number={getNumber(props.numberPlaced[3])}
                        onChange={(number) => props.onChange(3, number)}
                    />
                </Grid>
                <Grid item xs={4}>
                    <SudokuNumber
                        number={getNumber(props.numberPlaced[4])}
                        onChange={(number) => props.onChange(4, number)}
                    />
                </Grid>
                <Grid item xs={4}>
                    <SudokuNumber
                        number={getNumber(props.numberPlaced[5])}
                        onChange={(number) => props.onChange(5, number)}
                    />
                </Grid>
            </Grid>
            <Grid container item xs={4}>
                <Grid item xs={4}>
                    <SudokuNumber
                        number={getNumber(props.numberPlaced[6])}
                        onChange={(number) => props.onChange(6, number)}
                    />
                </Grid>
                <Grid item xs={4}>
                    <SudokuNumber
                        number={getNumber(props.numberPlaced[7])}
                        onChange={(number) => props.onChange(7, number)}
                    />
                </Grid>
                <Grid item xs={4}>
                    <SudokuNumber
                        number={getNumber(props.numberPlaced[8])}
                        onChange={(number) => props.onChange(8, number)}
                    />
                </Grid>
            </Grid>
        </Grid>
    )
}

export default SudokuRow
