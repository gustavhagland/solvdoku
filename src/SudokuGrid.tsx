import { Grid } from '@mui/material'
import { ReactElement, useEffect } from 'react'
import SudokuRow from './SudokuRow'

type Props = {
    /**
     * essentially xijk, i is row, j is column, k is number, xijk is {0,1}
     */
    numberPlaced: number[][][]
    onChange: (column: number, row: number, number: number | undefined) => void
}

const SudokuGrid = (props: Props): ReactElement => {
    useEffect(() => console.log(props.numberPlaced), [props.numberPlaced])
    return (
        <Grid container rowSpacing={2}>
            <Grid container item>
                <SudokuRow
                    numberPlaced={props.numberPlaced[0]}
                    onChange={(column, number) =>
                        props.onChange(column, 0, number)
                    }
                />
                <SudokuRow
                    numberPlaced={props.numberPlaced[1]}
                    onChange={(column, number) =>
                        props.onChange(column, 1, number)
                    }
                />
                <SudokuRow
                    numberPlaced={props.numberPlaced[2]}
                    onChange={(column, number) =>
                        props.onChange(column, 2, number)
                    }
                />
            </Grid>
            <Grid container item>
                <SudokuRow
                    numberPlaced={props.numberPlaced[3]}
                    onChange={(column, number) =>
                        props.onChange(column, 3, number)
                    }
                />
                <SudokuRow
                    numberPlaced={props.numberPlaced[4]}
                    onChange={(column, number) =>
                        props.onChange(column, 4, number)
                    }
                />
                <SudokuRow
                    numberPlaced={props.numberPlaced[5]}
                    onChange={(column, number) =>
                        props.onChange(column, 5, number)
                    }
                />
            </Grid>
            <Grid container item>
                <SudokuRow
                    numberPlaced={props.numberPlaced[6]}
                    onChange={(column, number) =>
                        props.onChange(column, 6, number)
                    }
                />
                <SudokuRow
                    numberPlaced={props.numberPlaced[7]}
                    onChange={(column, number) =>
                        props.onChange(column, 7, number)
                    }
                />
                <SudokuRow
                    numberPlaced={props.numberPlaced[8]}
                    onChange={(column, number) =>
                        props.onChange(column, 8, number)
                    }
                />
            </Grid>
        </Grid>
    )
}

export default SudokuGrid
