import { Autocomplete, TextField } from '@mui/material'
import { ReactElement } from 'react'

type Props = {
    number?: number
    onChange: (number?: number) => void
}
const numbers: { label: string; value: number | undefined }[] = [
    {
        label: '',
        value: undefined,
    },
    {
        label: '1',
        value: 1,
    },
    {
        label: '2',
        value: 2,
    },
    {
        label: '3',
        value: 3,
    },
    {
        label: '4',
        value: 4,
    },
    {
        label: '5',
        value: 5,
    },
    {
        label: '6',
        value: 6,
    },
    {
        label: '7',
        value: 7,
    },
    {
        label: '8',
        value: 8,
    },
    {
        label: '9',
        value: 9,
    },
]

const SudokuNumber = (props: Props): ReactElement => {
    return (
        <Autocomplete
            renderInput={(params) => <TextField {...params} />}
            options={numbers}
            value={props.number ? numbers[props.number] : numbers[0]}
            onChange={(_, val) => props.onChange(val?.value)}
        />
    )
}

export default SudokuNumber
