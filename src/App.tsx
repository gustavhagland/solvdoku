import { Button, Grid } from '@mui/material'
import React, { ReactElement, useState } from 'react'
import SudokuGrid from './SudokuGrid'

const App = (): ReactElement => {
    const [numberPlaced, setNumberPlaced] = useState<number[][][]>(
        new Array(9).fill(new Array(9).fill(new Array(9).fill(0)))
    )

    const onChange = (
        column: number,
        row: number,
        number: number | undefined
    ) => {
        const copy = numberPlaced
        const lastPlaced = numberPlaced[row][column].findIndex(
            (val) => val === 1
        )
        if (lastPlaced >= 0) {
            copy[row][column][lastPlaced] = 0
            console.log(lastPlaced + 1 + ' removed')
        }
        if (number) {
            copy[row][column][number - 1] = 1
            console.log(number + ' added')
        }
        setNumberPlaced(copy)
        console.log('setting numbers placed: ' + numberPlaced)
    }
    return (
        <Grid container spacing={2}>
            <Grid item xs={12} md={6}>
                <SudokuGrid numberPlaced={numberPlaced} onChange={onChange} />
            </Grid>
            <Grid item xs={12} md={6}>
                <Button>Solve</Button>
            </Grid>
        </Grid>
    )
}

export default App
